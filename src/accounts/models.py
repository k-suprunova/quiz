from django.db import models  # noqa

# Create your models here.
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    rating = models.IntegerField(null=False, default=0)

    def full_name(self):
        return f'{self.first_name} {self.last_name}'
