from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models import F

from accounts.models import User

# Create your models here.
from core.models import BaseModel
from core.utils import generate_uuid


class Test(models.Model):
    QUESTION_MIN_LIMIT = 1
    QUESTION_MAX_LIMIT = 20

    class LEVEL_CHOICES(models.IntegerChoices):
        BASIC = 0, "Basic"
        MIDDLE = 1, "Middle"
        ADVANCED = 2, "Advanced"

    # topic = models.ForeignKey(to=Topic, related_name='tests', null=True, on_delete=models.SET_NULL)
    uuid = models.UUIDField(default=generate_uuid, db_index=True)
    title = models.CharField(max_length=64)
    description = models.TextField(max_length=1024, null=True, blank=True)
    level = models.PositiveSmallIntegerField(choices=LEVEL_CHOICES.choices, default=LEVEL_CHOICES.MIDDLE)
    image = models.ImageField(default='default.png', upload_to='covers')

    def questions_count(self):
        return self.questions.count()

    def __str__(self):
        return f'{self.title}'

    def last_run(self):
        return self.results.last().create_date

    def best_result(self):
        if not self.results.all():
            return 'N/A'
        return self.results.order_by('num_incorrect_answers', F('write_date')-F('create_date')).first()

    def get_best_result(self):
        return self.best_result().num_correct_answers

    def get_best_user(self):
        return self.best_result().user


class Question(models.Model):
    ANSWER_MIN_LIMIT = 3
    ANSWER_MAX_LIMIT = 6

    test = models.ForeignKey(to=Test, related_name='questions', on_delete=models.CASCADE)
    order_number = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(Test.QUESTION_MIN_LIMIT),
                    MaxValueValidator(Test.QUESTION_MAX_LIMIT)])
    text = models.CharField(max_length=64)

    def __str__(self):
        return f'{self.text}'

    def is_last(self, order_number):
        return order_number == self.test.questions_count()


class Choice(models.Model):
    text = models.CharField(max_length=64)
    question = models.ForeignKey(to=Question, related_name='choices', on_delete=models.CASCADE)
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.text}'


class Result(BaseModel):
    class STATE(models.IntegerChoices):
        NEW = 0, "New"
        FINISHED = 1, "Finished"

    user = models.ForeignKey(to=User, related_name='results', on_delete=models.CASCADE)
    test = models.ForeignKey(to=Test, related_name='results', on_delete=models.CASCADE)
    state = models.PositiveSmallIntegerField(default=STATE.NEW, choices=STATE.choices)
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)

    num_correct_answers = models.PositiveSmallIntegerField(
        default=0,
        validators=[
            MaxValueValidator(Test.QUESTION_MAX_LIMIT)
        ]
    )
    num_incorrect_answers = models.PositiveSmallIntegerField(
        default=0,
        validators=[
            MaxValueValidator(Test.QUESTION_MAX_LIMIT)
        ]
    )

    def update_result(self, order_number, question, selected_choices):
        choices = question.choices.all()

        correct_answer = int(all(
            selected_choice == choices[i].is_correct
            for i, selected_choice in enumerate(selected_choices)
        ))

        self.num_correct_answers += correct_answer
        self.num_incorrect_answers += 1 - correct_answer
        self.current_order_number = question.order_number
        if question.is_last(order_number):
            self.state = self.STATE.FINISHED

        self.user.rating += self.points()
        self.user.save()

        self.save()

    def success_rate(self):
        return (self.num_correct_answers / self.test.questions_count())*100

    def time_spent(self):
        return self.write_date - self.create_date

    def points(self):
        res = self.num_correct_answers - self.num_incorrect_answers
        if res < 0:
            return 0
        return res
