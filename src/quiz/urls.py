from django.urls import path
from quiz.views import TestDetailView, TestListView, TestResultCreateView, TestResultUpdateView, TestResultDetailsView, \
  TestQuestionDetailsView, LeaderboardView

app_name = 'quizes'
urlpatterns = [
  path('', TestListView.as_view(), name='list'),
  path('leaderboard/', LeaderboardView.as_view(), name='leaderboard'),
  path('<uuid:uuid>/', TestDetailView.as_view(), name='details'),
  path('<uuid:uuid>/runs/create', TestResultCreateView.as_view(), name='create_run'),
  path('<uuid:uuid>/runs/<str:run_uuid>/continue', TestResultUpdateView.as_view(), name='continue_run'),
  path('<uuid:uuid>/runs/<str:run_uuid>/details', TestResultDetailsView.as_view(), name='run'),
  path('<uuid:uuid>/runs/<str:run_uuid>/questions/<int:order_number>', TestQuestionDetailsView.as_view(),
       name='question'),

]
