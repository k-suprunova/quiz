from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render  # noqa

# Create your views here.
from django.urls import reverse
from django.views.generic import ListView, DetailView, CreateView

from accounts.models import User
from quiz.forms import ChoiceFormSet
from quiz.models import Test, Result, Question


class TestListView(LoginRequiredMixin, ListView):
    model = Test
    template_name = 'tests/list.html'
    context_object_name = 'tests'


class TestDetailView(LoginRequiredMixin, DetailView):
    model = Test
    template_name = 'tests/details.html'
    context_object_name = 'test'
    pk_url_kwarg = 'uuid'

    def get_object(self):
        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uuid=uuid)


class TestResultCreateView(LoginRequiredMixin, CreateView):

    def post(self, request, *args, **kwargs):
        uuid = self.kwargs.get('uuid')
        test = Test.objects.get(uuid=uuid)
        user = self.request.user
        result = Result.objects.create(
            test=test,
            user=user,
            state=Result.STATE.NEW
        )
        result.save()

        return HttpResponseRedirect(reverse(
            'quizes:question',
            kwargs={
                'uuid': uuid,
                'run_uuid': result.uuid,
                'order_number': 1
            }
        ))


class TestResultDetailsView(LoginRequiredMixin, DetailView):
    model = Result
    template_name = 'results/details.html'

    def get_object(self):
        uuid = self.kwargs.get('run_uuid')
        return self.get_queryset().get(uuid=uuid)


class TestResultUpdateView(LoginRequiredMixin, DetailView):
    model = Result
    context_object_name = 'result'

    def get_object(self):
        uuid = self.kwargs.get('run_uuid')
        return self.get_queryset().get(uuid=uuid)

    def post(self, request, uuid, run_uuid):
        result = Result.objects.get(
            uuid=run_uuid,
        )

        return HttpResponseRedirect(reverse(
            'quizes:question',
            kwargs={
                'uuid': uuid,
                'run_uuid': run_uuid,
                'order_number': result.current_order_number or 1
            }
        ))


class TestQuestionDetailsView(LoginRequiredMixin, ListView):

    def get(self, request, uuid, run_uuid, order_number):  # noqa

        question = Question.objects.get(
            test__uuid=uuid,
            order_number=order_number
        )
        choices = question.choices.all()
        form_set = ChoiceFormSet(queryset=choices)

        result = render(
            request=request,
            template_name='tests/question.html',
            context={
                'question': question,
                'form_set': form_set,
            }
        )

        return result

    def post(self, request, uuid, run_uuid, order_number):  # noqa

        question = Question.objects.get(
            test__uuid=uuid,
            order_number=order_number
        )

        form_set = ChoiceFormSet(data=request.POST)

        selected_choices = [
            'is_selected' in form.changed_data
            for form in form_set.forms
        ]

        result = Result.objects.get(uuid=run_uuid)
        result.update_result(order_number, question, selected_choices)

        if question.is_last(order_number):
            return HttpResponseRedirect(
                reverse('quizes:run', kwargs={'uuid': uuid, 'run_uuid': run_uuid}))
        else:
            return HttpResponseRedirect(reverse(
                'quizes:question',
                kwargs={
                    'uuid': uuid,
                    'run_uuid': run_uuid,
                    'order_number': order_number+1
                }
            ))


class LeaderboardView(LoginRequiredMixin, ListView):
    model = User
    template_name = 'results/leaderboard.html'
    context_object_name = 'user'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['users'] = User.objects.all().order_by('-rating').filter(rating__gt=0)
        return context
